La documentation du *projet documentation* est mise à jour à l'adresse [](https://framagit.org/bib/pro/labim), il faut créer un compte sur framagit puis demander à rejoindre le groupe le bib [](https://framagit.org/bib) pour pouvoir éditer les pages soit en ligne, soit par clonage du projet sur un ordinateur, et envoi (push) des modifications. 

Il existe des éditeurs visuels comme *github desktop*, *gitkraken*  ou *gitcola*.

Pour utiliser le terminal, il y a ce guide : [](https://git-scm.com/book/fr/v2/Les-bases-de-Git-Travailler-avec-des-d%C3%A9p%C3%B4ts-distants)
