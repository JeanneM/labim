% Labim projet
% real@lebib.org
% Décembre 2018

# Labim 
Laboratoire d'imprimeries physique et numérique

## Labime constate

* l'univers de l'édition et de la publication sont en mutation,
* des outils de publication libres et efficaces existent depuis longtemps,
* une profusion d'outils existent.
---
*Changer d'outil implique de changer d'approche et de point de vue, générant des impulsions créatrices que nous souhaitons explorer*

## Labim expérimente 

les procédés d'impression 

* physiques,
* numérique,
* libres.

## Labim libére

* la circulation de l'information en se concentrant sur des process libre et reproductibles

## Labim documente

* ses recherches,
* ses travaux,
* ses outils de documentation.


## Labim @lebib

* Labim est ouverte à tous les matelots du bib.
* Labim se déroule généralement les jeudi aprés midi au bib, mais pas toujours et pas que.


## Labim collabore

* transversalement à tous les projets du bib ou d'ailleurs


## Labime ambitionne

* d'organiser le premier print & pint regroupant imprimeurs et brasseurs
* d'inviter prepostprint 


## Rejoignez Labim !

Mailing liste : labim@lebib.org
Wiki : https://lebib.org/wiki/doku.php?id=labim
Git : https://framagit.org/bib/pro/labim


## Impression envisageable

* Flyer de la G1 (monnaie libre) à destination des amap

