# OuDo : outils de documentation

Des outils de documentation simples, solides et efficaces sont nécessaires pour faire vivre et péréniser les actions menées au sein du BiB. 

Au delà de cette nécessité, le BiB a aussi pour vocation d'interroger et de nous permettre une compréhension et une maîtrise émancipatrice des outils que nous utilisons. 

Nous disposons d'un site web, d'un wiki et d'un dépot git que nous utilisons selon nos compétences. 

Nous proposons de consacrer un temps à interroger et penser ces outils afin de mieux les maîtriser et d'ainsi organiser et compléter la base documentaire de notre action.

Nous proposons aussi d'explorer d'autres pistes, des plus anciennes aux plus récentes, et d'ainsi multiplier les outils et leurs interconnections.




